public class TestAccount {

    public static void main(String[] args) {
        Account myAccount = new Account();
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {1,2,45,4,80};
        String[] names = {"Jane", "Joe","Bill","Bob","Tom"};

        myAccount.setName("Nadyah");
        myAccount.setBalance(200.60);
        System.out.println(myAccount.getName() + " has "+ myAccount.getBalance());

        for(int i=0; i<amounts.length;i++){
            amounts[i] = i+1;
            System.out.println(amounts[i]);
        }
        
    }
    
}